import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>GitLab</h1>
        <p>
          Lunch &amp; Learn Sessions
        </p>
      </header>
    </div>
  );
}

export default App;
