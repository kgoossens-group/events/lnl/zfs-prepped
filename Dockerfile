# build environment
FROM debian:9.8-slim

# Use Bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Install curl
RUN apt-get update && apt-get install -y curl

# Install Node.js
RUN curl -sL https://deb.nodesource.com/setup_13.x | bash -
RUN apt-get install -y nodejs

# Use NPM to install Yarn
RUN npm install --global yarn

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json yarn.lock /usr/src/app/
RUN yarn

COPY . /usr/src/app

RUN yarn build

ENV PORT 5000
EXPOSE $PORT
CMD [ "yarn", "prod" ]
